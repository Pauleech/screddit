# @screddit

### Scrapy Reddit Crawler under Random Proxy Middleware

## Tags

`crawling`, `python`, `scrapy`, `proxy`, `mongo`

## Introduction

In this project the [Reddit](https://www.reddit.com) crawler is presented. The scrapped data contains Reddit posts headers together with their additional information and is stored into .csv file or MongoDB. The crawler is made with the scrapy framework and allows to scrap data in parallel. This project also provides a script for obtaining free high anonymous proxies and Random Proxy Middleware. Thus, data can be collected not only obeying robots.txt file, but also using the IP address substitution with a proxy in automatic mode. The presented system can be used for the fast and safe text data collection and for its subsequent analysis.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes. 

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.0 Python 2.7 version;
* MongoDB 3.6.3 version;
* Scrapy 1.5: Web Crawling Framework for Python;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── ...
    ├── src                                     # project source
        ├── crawler                             # crawling approach
            ├── collecting                      # scrapy project
                ├── spiders                     # scrapy spiders
                ├── ...
            ├── ...
        ├── modules                             # additional modules of a common purpose
            ├── ...
    ├── config.py
    ├── main.py
    ├── ...    

`/src` and `/src/crawler/collecting` have to be marked in PyCharm as sources root folder.

### Glossary

* [Reddit](https://www.reddit.com) – an American social news aggregation, web content rating, and discussion website. Registered members submit content to the site such as links, text posts, and images, which are then voted up or down by other members. Posts are organized by subject into user-created boards called "subreddits", which cover a variety of topics including news, science, movies, video games, music, books, fitness, food, and image-sharing. Submissions with more up-votes appear towards the top of their subreddit and, if they receive enough votes, ultimately on the site's front page.

### Data

Project needs `subreddits.txt` file with Reddit subreddits to be scrapped.

Sample file can be downloaded [here](https://yadi.sk/i/Uqqn6Izl3TQEv9) and supposed to be kept in `/data` folder.

## Running the project

The main file is `main.py` located in the root folder.

Initially, it will collect available free highly anonymous proxies and after that will start to crawl Reddit.

By default, the following settings are used:

> Proxy usage is enabled.  
> `/src/crawler/collecting/setting.py`

```
DOWNLOADER_MIDDLEWARES = {
    "scrapy.downloadermiddlewares.retry.RetryMiddleware": 90,
    "modules.randomproxy.RandomProxy": 100,  # comment this row to turn proxies usage off.
    "scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware": 110,
}
```

> MongoDB pipeline is disabled.  
> `/src/crawler/collecting/setting.py`

```
ITEM_PIPELINES = {
    "collecting.pipelines.PreprocessPipeline": 300,
    "collecting.pipelines.CsvPipeline": 500,
    # "collecting.pipelines.MongoDBPipeline": 800,  # uncomment this row to turn on MongoDB pipeline.
}
```

> Crawling depth limit is 2.  
> `/src/crawler/collecting/setting.py`

```
DEPTH_LIMIT = 2
```

> All commas are replaced with a semicolon for the `.csv` file.

The result of the script will contain data consisting of 6 fields:

<p align="center">
  <img src="/uploads/792ee7da0fc7ca237607b9b9a56d434a/csv.png" width="800px" >
</p>
