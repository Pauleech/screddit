import os
import sys
import logging
import platform
import multiprocessing
from modules.logering import setup_logger
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding("utf-8")
sys.stdout = stdout

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_path = os.path.join(root_path, "data")
crawler_folder = os.path.join(root_path, "src/crawler")

# files
proxy_filename = os.path.join(data_path, "proxy.txt")
subreddits_filename = os.path.join(data_path, "subreddits.txt")
data_filename = os.path.join(data_path, "data.csv")

# seed
seed = 42

# logging
log_level = logging.INFO
logger = setup_logger("logger", log_level)

# crawler
crawler_fields = ["id", "title", "subreddit", "date", "link", "vote"]

