from scrapy import cmdline


def run_scraping():
    """
    Runs crawler.
    """
    command = "scrapy crawl screddit --loglevel DEBUG --logfile screddit.log"
    cmdline.execute(command.split())
