# -*- coding: utf-8 -*-
import config
import pymongo
from dateutil import parser
from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy.exporters import CsvItemExporter


class PreprocessPipeline(object):
    def __init__(self):
        """
        Initiates PreprocessPipeline.
        """
        self.ids_seen = set()

    def process_item(self, item, spider):
        """
        Processes an item for further tasks.
        """
        # check item consistency
        for field in config.crawler_fields:
            if field not in item:
                raise DropItem("Missing {}!".format(field))
        # check item vote and date
        if item["vote"] == u"\u2022":
            item["vote"] = "hidden"
        else:
            item["vote"] = int(item["vote"])
        item["date"] = parser.parse(item["date"])
        # check inner link
        if "http" not in item["link"]:
            item["link"] = "https://www.reddit.com{}".format(item["link"])
        # check duplicate
        if item["id"] in self.ids_seen:
            raise DropItem("Duplicate item found: {}".format(item))
        else:
            self.ids_seen.add(item["id"])
        return item


class MongoDBPipeline(object):
    def __init__(self):
        """
        Initiates MongoDBPipeline.
        """
        connection = pymongo.MongoClient(
            settings["MONGODB_SERVER"],
            settings["MONGODB_PORT"]
        )
        db = connection[settings["MONGODB_DB"]]
        self.collection = db[settings["MONGODB_COLLECTION"]]

    def process_item(self, item, spider):
        """
        Puts an item to MongoDB.
        """
        self.collection.insert(dict(item))
        return item


class CsvPipeline(object):
    def __init__(self):
        """
        Initiates CsvPipeline.
        """
        self.file = open(config.data_filename, "wb")
        self.exporter = CsvItemExporter(self.file, unicode)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        """
        Closes csv file at the end of exporting.
        """
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        """
        Exports an item to csv file.
        """
        for key, value in item.items():
            is_string = (isinstance(value, basestring))
            if is_string and ("," in value.encode("utf-8")):
                item[key] = value.encode("utf-8").replace(",", ";")
        self.exporter.export_item(item)
        return item
