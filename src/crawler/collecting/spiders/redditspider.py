import config
from scrapy import Request
from items import RedditItem
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class RedditSpider(CrawlSpider):
    name = "screddit"
    allowed_domains = ["reddit.com"]
    website = "https://www.reddit.com"
    subreddits = [line[:line.rfind("/")+1] for line in open(config.subreddits_filename, "r").readlines()]
    start_urls = [website + subr for subr in subreddits]
    allowed_masks = ["/r/\w*/\?count=\d*&after=\w*"]

    rules = [Rule(LinkExtractor(
        allow=allowed_masks),
        callback="parse_items",
        follow=True)]

    def parse_items(self, response):
        """
        Main parsing script.
        """
        if not response.xpath("/html/head/title"):
            self.logger.error("Used IP/Proxy failed")
            yield Request(url=response.url, dont_filter=True)
        selector_list = response.css("div.thing")
        for selector in selector_list:
            item = RedditItem()
            try:
                item["id"] = selector.xpath("@data-fullname").extract()[0]
                item["title"] = selector.xpath("div[2]/div/p/a/text()").extract()[0]
                item["subreddit"] = selector.xpath("@data-subreddit").extract()[0]
                item["date"] = selector.xpath("div[2]/div/p[2]/time/@title").extract()[0]
                item["link"] = selector.xpath("@data-url").extract()[0]
                try:
                    item["vote"] = selector.xpath("div[1]/div[3]/@title").extract()[0]
                except IndexError:
                    item["vote"] = selector.xpath("div[1]/div[3]/text()").extract()[0]
                yield item
            except Exception as e:
                self.logger.error(e)
