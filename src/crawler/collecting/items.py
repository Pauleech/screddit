# -*- coding: utf-8 -*-
from scrapy import Item, Field


class RedditItem(Item):
    id = Field()
    title = Field()
    date = Field()
    link = Field()
    vote = Field()
    subreddit = Field()

