import requests
from lxml import html


def get_proxy_list():
    # type: () -> list
    """
    Returns a list with free highly anonymous proxies.

    :rtype: list
    :return: list with proxies.
    """
    page = requests.get("https://www.ip-adress.com/proxy-list")
    tree = html.fromstring(page.content)
    proxies_ip = tree.xpath("//tr/td[1]/a/text()")
    proxies_ports = tree.xpath("//tr/td[1]/text()")
    proxies_types = tree.xpath("//tr/td[2]/text()")
    proxies_dates = tree.xpath("//tr/td[4]/time/text()")
    output = ["http://{}{}".format(ip, port)
              for ip, port, ip_type, date in zip(proxies_ip, proxies_ports, proxies_types, proxies_dates)
              if ip_type == "highly-anonymous" and ("hour" in date or "minute" in date)]
    return output


if __name__ == "__main__":
    print get_proxy_list()
