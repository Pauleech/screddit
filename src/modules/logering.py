import logging
from functools import wraps


def setup_logger(name, level):
    # type: (str, int) -> logger
    """
    Setups logger and logfile.

    :rtype: logger
    :param name: logger name.
    :param level: log level.
    :return: logger.
    """
    logger = logging.getLogger(name)
    logging.basicConfig(level=level)
    return logger


def logit(logger, message):
    """
    Logging decorator.
    """

    def outer(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            response = fn(*args, **kwargs)
            logger.info("..{} completed.".format(message))
            return response

        return inner

    return outer
