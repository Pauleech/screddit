import os
import config
from crawler.scripts import run_scraping
from modules.timing import elapsed_timer
from modules.freeproxy import get_proxy_list


def main():
    """
    Module main function.
    """
    with elapsed_timer(config.logger, "getting proxies", title=True):
        proxy_list = get_proxy_list()
        with open(config.proxy_filename, "w") as f:
            f.writelines(["{}\n".format(proxy) for proxy in proxy_list])
        config.logger.info("..proxies found: {}".format(len(proxy_list)))
    os.chdir(config.crawler_folder)
    run_scraping()


if __name__ == "__main__":
    main()
